const container = document.querySelector(".container");

const inputForm = document.querySelector(".input-new");
inputForm.classList.add("hide-input");

function addNew() {
    var x = document.getElementById("myDIV");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}   

function createDrop() {
  const drop = document.createElement("div");
  drop.className = "drop";
  drop.style.left = Math.random() * container.offsetWidth + "px";
  container.appendChild(drop);
  setTimeout(() => {
    drop.remove();
  }, 1000);
}

setInterval(createDrop, 10);